import { Injectable } from '@angular/core';
import { Video } from './video.service';

@Injectable()
export class AppStateService {

  // this is not react
  videoList: Video[] = [];
  activeVideo: Video;
  constructor() { }

}
