import { Component, Input } from '@angular/core';
import { Video } from '../video.service';
import { AppStateService } from '../app-state.service';

@Component({
  selector: 'app-youtube-item',
  templateUrl: './youtube-item.component.html',
  styleUrls: ['./youtube-item.component.css']
})

export class YoutubeItemComponent {

  @Input() video: Video;

  constructor(private appState: AppStateService) { }

  onClick() {
    this.appState.activeVideo = this.video;
  }
}
