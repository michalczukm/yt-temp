import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import { YoutubeNavComponent } from './youtube-nav/youtube-nav.component';
import { YoutubeListComponent } from './youtube-list/youtube-list.component';
import { YoutubeItemComponent } from './youtube-item/youtube-item.component';
import { YoutubeService } from './youtube.service';
import { AppStateService } from './app-state.service';

@NgModule({
    declarations: [
        AppComponent,
        YoutubeNavComponent,
        YoutubeListComponent,
        YoutubeItemComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    providers: [
      YoutubeService,
      AppStateService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
