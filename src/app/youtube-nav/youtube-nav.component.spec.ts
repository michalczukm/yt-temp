import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeNavComponent } from './youtube-nav.component';

describe('YoutubeNavComponent', () => {
  let component: YoutubeNavComponent;
  let fixture: ComponentFixture<YoutubeNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutubeNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
