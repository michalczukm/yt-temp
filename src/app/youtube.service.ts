import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { RequestOptionsArgs } from '@angular/http/src/interfaces';

@Injectable()
export class YoutubeService {

  constructor(private http: Http) { }

  fetchVideos(query: string) {
    const options = {
      params: {
        q: query,
        type: 'video',
        maxResults: 25,
        part: 'snippet',
        key: 'AIzaSyDW_ZgXmDGaQR9V6oxy6OgR89Fw2VRBG4k'
      }
    } as RequestOptionsArgs;

    return this.http
      .get('https://content.googleapis.com/youtube/v3/search', options)
      .map(response => response.json());
  }
}
