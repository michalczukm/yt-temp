import { Component, OnInit } from '@angular/core';
import { YoutubeService } from '../youtube.service';
import { AppStateService } from '../app-state.service';
import * as moment from 'moment';
import { Video } from '../video.service';

@Component({
  selector: 'app-youtube-list',
  templateUrl: './youtube-list.component.html',
  styleUrls: ['./youtube-list.component.css']
})
export class YoutubeListComponent implements OnInit {

  videoList: Video[] = [];

  constructor(private youtubeservice: YoutubeService, private appState: AppStateService) { }

  ngOnInit() {
    this.youtubeservice.fetchVideos('typescript')
      .subscribe(data => {
        this.videoList = data.items.map(item => {
          return new Video(
            item.id.videoId,
            item.snippet.title,
            item.snippet.thumbnails.high.url,
            item.snippet.channelTitle,
            item.snippet.channelId,
            moment(item.snippet.publishedAt).fromNow(),
            item.snippet.description);
        });

        // this is not react, nor redux
        this.appState.videoList = this.videoList;
        this.appState.activeVideo = this.appState.videoList[0];
      });
  }

}
